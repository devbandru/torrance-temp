from django.contrib import admin
from .models import LXDNode, LXDImage
from .lxd_client import client

@admin.register(LXDNode)
class LXDNodeAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'endpoint', 'added')

@admin.register(LXDImage)
class LXDImageAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'osversion', 'added')


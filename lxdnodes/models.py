import uuid
from django.db import models
from django.utils import timezone

class LXDNode(models.Model):
    name = models.CharField(max_length=250)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    endpoint = models.CharField(max_length=250)
    password = models.CharField(max_length=250)
    added = models.DateTimeField(auto_now_add=True)
    verify = models.BooleanField(default=False)

    class Meta:
        ordering = ('-added',)

    def __str__(self):
        return self.name

class LXDImage(models.Model):
    name = models.CharField(max_length=250)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    alias = models.CharField(max_length=250, default=None)
    osversion = models.CharField(max_length=250)
    url = models.URLField()
    added = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-added',)

    def __str__(self):
        return self.name

from pylxd import Client
import os
import time

class client:

    basedir = os.path.dirname(__file__)
    cert = os.path.join(basedir, 'ssl/lxd.crt')
    key = os.path.join(basedir, 'ssl/lxd.key')
    cert = (cert, key)


    def __init__(self, endpoint, password, verify):
        self.endpoint = endpoint
        self.password = password
        self.verify = verify
#        self.cert = cert

    def connect(endpoint=None, cert=cert, password=None, verify=True):
        client = Client(
            endpoint=endpoint,
            cert=cert,
            verify=verify)
        client.authenticate(password)
        return client

    def create_container(client, name=None, image=None, profile='default'):
        config = {'name': name,
                  'source':
                      {'type': 'image',
                       'mode': 'pull',
                       'server': 'https://cloud-images.ubuntu.com/daily',
                       'protocol': "simplestreams",
                       'alias': image}}
        container = client.containers.create(config, wait=True)
        container.start()

    def stop_container(client, name=None):
        container = client.containers.get(name)
        container.stop()

    def start_container(client, name=None):
        container = client.containers.get(name)
        container.start()

    def restart_container(client, name=None):
        container = client.containers.get(name)
        container.restart()

    def delete_container(client, name=None):
        container = client.containers.get(name)
        try:
            container.stop(wait=True)
        except:
            pass

        try:
            container.delete(wait=True)
        except:
            pass

    def get_container_state(client, name=None):
        container = client.containers.get(name)
        container.state()

    def freeze_container(client, name=None):
        container = client.containers.get(name)
        container.freeze()

    def unfreeze_container(client, name=None):
        container = client.containers.get(name)
        container.unfreeze()

    def migrate_container(client, name=None, dest=None, live=None):
        container = client.containers.get(name)
        container.migrate(dest, live)

    def get_ip_addr(client, name=None, interface='eth0'):
        container = client.containers.get(name)
        while True:
            addr_scope = container.state().network[interface]['addresses'][0]['scope']
            print(addr_scope)
            if add_scope == 'global':
                ip = container.state().network[interface]['addresses'][0]['address']
                return ip
                break
            else:
                continue

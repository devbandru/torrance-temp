from django.apps import AppConfig


class AppsessionsConfig(AppConfig):
    name = 'appsessions'

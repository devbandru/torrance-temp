# Generated by Django 3.1.3 on 2020-11-23 19:23

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='LXDNode',
            fields=[
                ('name', models.CharField(max_length=250)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('endpoint', models.CharField(max_length=250)),
                ('password', models.CharField(max_length=250)),
                ('added', models.DateTimeField(auto_now_add=True)),
                ('verify', models.BooleanField(default=False)),
            ],
            options={
                'ordering': ('-added',),
            },
        ),
        migrations.CreateModel(
            name='AppSession',
            fields=[
                ('name', models.CharField(max_length=250)),
                ('id', models.UUIDField(default=uuid.uuid4, editable=False, primary_key=True, serialize=False)),
                ('type', models.CharField(choices=[('xpra', 'Xpra'), ('rdp_remoteapp', 'RDP RemoteApp')], max_length=13)),
                ('image', models.CharField(max_length=200)),
                ('created', models.DateTimeField(auto_now_add=True)),
                ('updated', models.DateTimeField(auto_now=True)),
                ('status', models.CharField(choices=[('added', 'Added'), ('created', 'Created'), ('running', 'Running'), ('stopped', 'Stopped')], default='Added', max_length=7)),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='appsessions', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
    ]

# Generated by Django 3.1.3 on 2020-11-23 19:47

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('appsessions', '0001_initial'),
    ]

    operations = [
        migrations.DeleteModel(
            name='LXDNode',
        ),
    ]

from django.contrib import admin
from .models import AppSession
from lxdnodes.lxd_client import client

def create_appsession(modeladmin, request, queryset):
    session = queryset.first()
    name = session.name
    endpoint = session.node.endpoint
    password = session.node.password
    image = session.image.alias
    verify = session.node.verify
    lxd_client = client.connect(endpoint=endpoint,
                                password=password,
                                verify=verify)
    client.create_container(client=lxd_client,
                                name=name,
                                image=image)
    queryset.update(status='running')
    ipaddr = client.get_ip_addr(client=lxd_client,
                                    name=name)
    queryset.update(ip=ipaddr)

def stop_appsession(modeladmin, request, queryset):
    session = queryset.first()
    name = session.name
    endpoint = session.node.endpoint
    password = session.node.password
    verify = session.node.verify
    lxd_client = client.connect(endpoint=endpoint,
                                password=password,
                                verify=verify)
    client.stop_container(client=lxd_client,
                          name=name)
    queryset.update(status='stopped')

def delete_appsession(modeladmin, request, queryset):
    session = queryset.first()
    name = session.name
    endpoint = session.node.endpoint
    password = session.node.password
    verify = session.node.verify
    lxd_client = client.connect(endpoint=endpoint,
                                password=password,
                                verify=verify)
    client.delete_container(client=lxd_client,
                            name=name)
    queryset.update(status='deleted')

def start_appsession(modeladmin, request, queryset):
    session = queryset.first()
    if session.status == 'stopped':
        name = session.name
        endpoint = session.node.endpoint
        password = session.node.password
        verify = session.node.verify
        lxd_client = client.connect(endpoint=endpoint,
                                    password=password,
                                    verify=verify)
        client.start_container(client=lxd_client,
                               name=name)
        queryset.update(status='running')
        ipaddr = client.get_ip_addr(client=lxd_client,
                                    name=name)
        queryset.update(ip=ipaddr)
    else:
        pass

def restart_appsession(modeladmin, request, queryset):
    session = queryset.first()
    if session.status == 'running':
        name = session.name
        endpoint = session.node.endpoint
        password = session.node.password
        verify = session.node.verify
        lxd_client = client.connect(endpoint=endpoint,
                                    password=password,
                                    verify=verify)
        client.start_container(client=lxd_client,
                               name=name)
        queryset.update(status='running')
        ipaddr = client.get_ip_addr(client=lxd_client,
                                    name=name)
        queryset.update(ip=ipaddr)
    else:
        pass

def restart_appsession(modeladmin, request, queryset):
    session = queryset.first()
    name = session.name
    endpoint = session.node.endpoint
    password = session.node.password
    verify = session.node.verify
    lxd_client = client.connect(endpoint=endpoint,
                                password=password,
                                verify=verify)
    client.start_container(client=lxd_client,
                           name=name)
    queryset.update(status='deleted')

def freeze_appsession(modeladmin, request, queryset):
    session = queryset.first()
    name = session.name
    endpoint = session.node.endpoint
    password = session.node.password
    verify = session.node.verify
    lxd_client = client.connect(endpoint=endpoint,
                                password=password,
                                verify=verify)
    client.freeze_container(client=lxd_client,
                            name=name)
    queryset.update(status='freezed')

def unfreeze_appsession(modeladmin, request, queryset):
    session = queryset.first()
    if session.status == 'freezed':
        name = session.name
        endpoint = session.node.endpoint
        password = session.node.password
        verify = session.node.verify
        lxd_client = client.connect(endpoint=endpoint,
                                    password=password,
                                    verify=verify)
        client.freeze_container(client=lxd_client,
                                name=name)
        queryset.update(status='running')
    else:
        pass

create_appsession.short_description = 'Create AppSession'
start_appsession.short_description = 'Start AppSession'
stop_appsession.short_description = 'Stop AppSession'
freeze_appsession.short_description = 'Freeze AppSession'
unfreeze_appsession.short_description = 'Unfreeze AppSession'
delete_appsession.short_description = 'Delete AppSession'

@admin.register(AppSession)
class AppSessionAdmin(admin.ModelAdmin):
    list_display = ('name', 'id', 'user', 'image', 'created', 'status', 'ip')
    list_filter = ('status', 'created', 'name', 'ip')
    search_fields = ('name', 'id', 'user__username', 'image', 'created', 'ip')
    ordering = ('name', 'created', 'status', 'user', 'image__name', 'ip')
    actions = [create_appsession,
               start_appsession,
               stop_appsession,
               freeze_appsession,
               unfreeze_appsession,
               delete_appsession]

from django.urls import path
from . import views

app_name = 'appsessions'

urlpatterns = [
    path('', views.appsession_list, name='appsession_list'),
]

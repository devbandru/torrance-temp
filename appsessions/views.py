from django.shortcuts import render, get_object_or_404
from .models import AppSession

def appsession_list(request):
    appsessions = AppSession.objects.all()
    return render(request,
                  'appsessions/list.html',
                  {'appsessions': appsessions})

# Create your views here.

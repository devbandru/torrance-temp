import uuid
import os
from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User
from lxdnodes.models import LXDNode, LXDImage

class AppSession(models.Model):
    # Список состояний сессий необходимо брать с бэкенда
    STATUS_LIST = (
        ('added', 'Added'),
        ('created', 'Created'),
        ('running', 'Running'),
        ('stopped', 'Stopped'),
        ('freezed', 'Freezed'),
        ('deleted', 'Deleted'),
    )

    SESSION_TYPES = (
        ('xpra', 'Xpra'),
        ('rdp_remoteapp', 'RDP RemoteApp'),
    )

    name = models.CharField(max_length=250)
    id = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    user = models.ForeignKey(User,
                             on_delete=models.CASCADE,
                             related_name='appsessions')
    type = models.CharField(max_length=13,
                            choices=SESSION_TYPES)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    status = models.CharField(max_length=7,
                              choices=STATUS_LIST,
                              default="Added")
    node = models.ForeignKey(LXDNode,
                             on_delete=models.CASCADE)
    image = models.ForeignKey(LXDImage,
                              on_delete=models.CASCADE,
			      null=True)
    ip = models.GenericIPAddressField(null=True, blank=True)

    class Meta:
        ordering = ('-created',)

    def __str__(self):
        return self.name
